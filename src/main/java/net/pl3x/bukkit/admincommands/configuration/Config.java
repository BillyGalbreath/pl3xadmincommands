package net.pl3x.bukkit.admincommands.configuration;

import net.pl3x.bukkit.admincommands.Main;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml");

    private Main plugin;
    private final Object def;

    private Config(Object def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
    }

    public String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }
}
