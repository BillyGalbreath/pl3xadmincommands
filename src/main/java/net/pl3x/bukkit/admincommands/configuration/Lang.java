package net.pl3x.bukkit.admincommands.configuration;

import net.pl3x.bukkit.admincommands.Logger;
import net.pl3x.bukkit.admincommands.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    PLAYER_COMMAND("&4This command is only available to players."),
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!");

    private Main plugin;
    private String def;

    private File configFile;
    private FileConfiguration config;

    Lang(String def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
        configFile = new File(plugin.getDataFolder(), Config.LANGUAGE_FILE.getString());
        saveDefault();
        reload();
    }

    public void reload() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private void saveDefault() {
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE.getString(), false);
        }
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(getKey(), def);
        if (value == null) {
            Logger.error("Missing lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
