package net.pl3x.bukkit.admincommands;

import net.pl3x.bukkit.admincommands.commands.CmdSetSpawn;
import net.pl3x.bukkit.admincommands.commands.CmdSpawn;
import net.pl3x.bukkit.admincommands.listeners.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import java.io.IOException;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
        } catch (IOException e) {
            Logger.warn("&4Failed to start Metrics: &e" + e.getMessage());
        }

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled!");
    }
}
